﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsInput;
using WindowsInput.Native;

namespace FuckBot.Operations
{
    class TerrariaTestOperation : IBotOperation
    {
        public IEnumerable<string> Keywords { get; }
        
        public string Name => "Terraia";
        public string Description => "Opens inventory!";

        public TerrariaTestOperation(IEnumerable<string> keyword)
        {
            Keywords = keyword;
        }

        public void Run()
        {
            Console.WriteLine("Executed drop items!");

            SendKeys.Send("{ESC}");
        }
    }
}
