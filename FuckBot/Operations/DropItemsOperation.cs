﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsInput;
using WindowsInput.Native;

namespace FuckBot.Operations
{
    class DropItemsOperation : IBotOperation
    {
        public IEnumerable<string> Keywords { get; }
        
        public string Name => "DropItems";
        public string Description => "Drop items for phasmophobia";

        public DropItemsOperation(IEnumerable<string> keyword)
        {
            Keywords = keyword;
        }

        public void Run()
        {
            Console.WriteLine("Executed drop items!");

            SendKeys.Send("SENdD CHICKENS!");
        }
    }
}
