﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsInput;
using WindowsInput.Native;

namespace FuckBot.Operations
{
    class ChangeWeaponOperation : IBotOperation
    {
        private readonly InputSimulator _inputSimulator;

        public IEnumerable<string> Keywords { get; }
        public string Name => "Changes weapon";
        public string Description => "Changes the Weapon!";


        public ChangeWeaponOperation(IEnumerable<string> keyword)
        {
            Keywords = keyword;
            _inputSimulator = new InputSimulator();
        }


        public void Run()
        {
            Console.WriteLine("Changed the weapon!");

            Random r = new Random();

            _inputSimulator.Mouse.VerticalScroll(r.Next(-3, 4));
        }

    }
}
