﻿using System.Collections.Generic;
using WindowsInput;

namespace FuckBot
{
    public interface IBotOperation
    {
        string Name { get; }
        IEnumerable<string> Keywords { get; }
        string Description { get; }
        void Run();
    }
}
