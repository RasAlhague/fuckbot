﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FuckBot.Gui
{
    public partial class MemeMeForm : Form
    {
        public MemeMeForm(string imagePath)
        {
            InitializeComponent();

            var image = new Bitmap(imagePath);
            Width = image.Width;
            Height = image.Height;

            BackgroundImage = image;
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
