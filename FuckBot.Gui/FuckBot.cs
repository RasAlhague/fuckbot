﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Speech.Recognition;

namespace FuckBot
{
    public delegate void FuckDetectedHandler(string name, string keyword, string description);

    public class FuckBot : IDisposable
    {
        private readonly SpeechRecognitionEngine _recognizer;
        private OperationBundle _operationBundle;
        private bool _isRunning;
        public bool IsRunning => _isRunning;

        public string BundleName => _operationBundle.BundleName;

        public event FuckDetectedHandler FuckDetected;

        public FuckBot(CultureInfo cultureInfo, OperationBundle operationBundle)
        {
            SpeechRecognitionEngine recognizer = new SpeechRecognitionEngine(cultureInfo);
            recognizer.LoadGrammar(new DictationGrammar());
            recognizer.LoadGrammar(operationBundle.BuildGrammar());
            recognizer.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(OnRecognized);
            recognizer.SetInputToDefaultAudioDevice();

            _recognizer = recognizer;
            _operationBundle = operationBundle;
            _isRunning = false;
        }

        public void ChangeBundle(OperationBundle bundle)
        {
            if (IsRunning)
            {
                _recognizer.RecognizeAsyncStop();
            }

            _operationBundle = bundle;

            _recognizer.UnloadAllGrammars();
            _recognizer.LoadGrammar(new DictationGrammar());
            _recognizer.LoadGrammar(bundle.BuildGrammar());

            if (IsRunning)
            {
                _recognizer.RecognizeAsync(RecognizeMode.Multiple);
            }
        }

        private void OnRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            Debug.WriteLine("Voice recognized...");
            Debug.WriteLine(e.Result.Text);

            foreach (var operation in _operationBundle.BotOperations.Where(x => x.Keywords.Any(y => e.Result.Text.ToLower().Contains(y.ToLower()))))
            {
                operation.Run();

                OnFuckDetected(operation.Name, e.Result.Text, operation.Description);
            }
        }

        public void Run()
        {
            Console.WriteLine("Fuckbot running...");
            _recognizer.RecognizeAsync(RecognizeMode.Multiple);
            _isRunning = true;
        }

        public void Stop()
        {
            Console.WriteLine("Fuckbot stopping...");
            _recognizer.RecognizeAsyncStop();
            _isRunning = false;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _recognizer.Dispose();
        }

        protected void OnFuckDetected(string name, string keyword, string description)
        {
            FuckDetected?.Invoke(name, keyword, description);
        }
    }
}
