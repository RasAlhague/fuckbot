﻿using Newtonsoft.Json;
using System.IO;

namespace FuckBot
{
    class Config
    {
        public string MemeFileFolder { get; set; }
        public string ScarySoundsFolder { get; set; }
        public string DefaultBundleFolder { get; set; }

        public Config()
        {
            MemeFileFolder = "./memes";
            ScarySoundsFolder = "./sounds";
            DefaultBundleFolder = "./bundles/default.json";
        }

        public void WriteConfig(string path)
        {
            string json = JsonConvert.SerializeObject(this);

            File.WriteAllText(path, json);
        }

        public static Config GetConfig(string path)
        {
            Config config = new Config();

            if (!File.Exists(path))
            {
                config.WriteConfig(path);
            }
            else
            {
                string file = File.ReadAllText(path);

                config = JsonConvert.DeserializeObject<Config>(file);
            }

            return config;
        }
    }
}
