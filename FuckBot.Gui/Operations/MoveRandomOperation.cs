﻿using System;
using System.Collections.Generic;
using System.Threading;
using WindowsInput;
using WindowsInput.Native;

namespace FuckBot.Gui.Operations
{
    class MoveRandomOperation : IBotOperation
    {
        private readonly InputSimulator _inputSimulator;

        public IEnumerable<string> Keywords { get; }
        public string Name => "Move Random";
        public string Description => "Lets the player move around randomly!";


        public MoveRandomOperation(IEnumerable<string> keyword)
        {
            Keywords = keyword;
            _inputSimulator = new InputSimulator();
        }

        public void Run()
        {
            Console.WriteLine("Play scary sound!");

            Random r = new Random();


            _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.VK_W);
            Thread.Sleep(r.Next(100, 1000));
            _inputSimulator.Keyboard.KeyUp(VirtualKeyCode.VK_W);

            _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.VK_A);
            Thread.Sleep(r.Next(100, 1000));
            _inputSimulator.Keyboard.KeyUp(VirtualKeyCode.VK_A);

            _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.VK_S);
            Thread.Sleep(r.Next(100, 1000));
            _inputSimulator.Keyboard.KeyUp(VirtualKeyCode.VK_S);

            _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.VK_D);
            Thread.Sleep(r.Next(100, 1000));
            _inputSimulator.Keyboard.KeyUp(VirtualKeyCode.VK_D);

        }

    }
}
