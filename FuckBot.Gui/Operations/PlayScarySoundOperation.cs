﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace FuckBot.Gui.Operations
{
    class PlayScarySoundOperation : IBotOperation
    {
        private WaveOutEvent _outputDevice;
        private AudioFileReader _audioFile;

        public IEnumerable<string> Keywords { get; }
        public string Name => "Scarry sound";
        public string Description => "Plays a scarry sound when i curse!";


        public PlayScarySoundOperation(IEnumerable<string> keyword)
        {
            Keywords = keyword;
        }

        private void OnPlaybackStopped(object sender, StoppedEventArgs e)
        {
            _outputDevice.Dispose();
            _audioFile.Dispose();
        }

        public void Run()
        {
            Console.WriteLine("Play scary sound!");

            Random r = new Random();
            Thread.Sleep(r.Next(500, 5000));


            string fileBasePath = Config.GetConfig(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "fuckbot_config.json")).ScarySoundsFolder;

            var files = Directory.GetFiles(fileBasePath, "*.mp3").ToList();
            string file = files[r.Next(0, files.Count)];

            _outputDevice = new WaveOutEvent();
            _audioFile = new AudioFileReader(file);
            _outputDevice.PlaybackStopped += OnPlaybackStopped;
            _outputDevice.Volume = 0.30f;

            _outputDevice.Init(_audioFile);

            _outputDevice.Play();
        }

    }
}
