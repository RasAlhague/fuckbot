﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FuckBot.Gui.Operations
{
    class MemeMeOperation : IBotOperation
    {
        public IEnumerable<string> Keywords { get; }
        public string Name => "Changes weapon";
        public string Description => "Changes the Weapon!";


        public MemeMeOperation(IEnumerable<string> keyword)
        {
            Keywords = keyword;
        }


        public void Run()
        {
            Console.WriteLine("Changed the weapon!");

            string memeFolder = Config.GetConfig(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "fuckbot_config.json")).MemeFileFolder;

            Random r = new Random();

            var files = Directory.GetFiles(memeFolder);

            MemeMeForm memeMeForm = new MemeMeForm(files[r.Next(0, files.Length)]);
            memeMeForm.Show();
        }

    }
}
