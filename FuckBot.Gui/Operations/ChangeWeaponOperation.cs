﻿using System;
using System.Collections.Generic;
using WindowsInput;

namespace FuckBot.Gui.Operations
{
    class ChangeWeaponOperation : IBotOperation
    {
        private readonly InputSimulator _inputSimulator;

        public IEnumerable<string> Keywords { get; }
        public string Name => "Changes weapon";
        public string Description => "Changes the Weapon!";


        public ChangeWeaponOperation(IEnumerable<string> keyword)
        {
            Keywords = keyword;
            _inputSimulator = new InputSimulator();
        }


        public void Run()
        {
            Console.WriteLine("Changed the weapon!");

            Random r = new Random();

            _inputSimulator.Mouse.VerticalScroll(r.Next(-3, 4));
        }

    }
}
