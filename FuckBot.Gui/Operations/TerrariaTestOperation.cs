﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FuckBot.Gui.Operations
{
    class TerrariaTestOperation : IBotOperation
    {
        public IEnumerable<string> Keywords { get; }
        
        public string Name => "Terraia";
        public string Description => "Opens inventory!";

        public TerrariaTestOperation(IEnumerable<string> keyword)
        {
            Keywords = keyword;
        }

        public void Run()
        {
            Console.WriteLine("Executed drop items!");

            SendKeys.Send("{ESC}");
        }
    }
}
