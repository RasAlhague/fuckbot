﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;
using WindowsInput.Native;

namespace FuckBot.Gui.Operations.Outlast
{
    class RotatePlayerOperation : IBotOperation
    {
        private readonly InputSimulator _inputSimulator;

        public IEnumerable<string> Keywords { get; }
        public string Name => "Rotate Player";
        public string Description => "Rotates the player from left to right!";


        public RotatePlayerOperation(IEnumerable<string> keyword)
        {
            Keywords = keyword;
            _inputSimulator = new InputSimulator();
        }

        public void Run()
        {
            Console.WriteLine("Reload the Camera!");

            Random r = new Random();

            _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.VK_G);
            Thread.Sleep(r.Next(10000));
            _inputSimulator.Keyboard.KeyUp(VirtualKeyCode.VK_G);
            _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.VK_H);
            Thread.Sleep(r.Next(10000));
            _inputSimulator.Keyboard.KeyUp(VirtualKeyCode.VK_H);
        }
    }
}
