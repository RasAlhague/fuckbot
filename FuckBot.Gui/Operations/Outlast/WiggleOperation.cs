﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;
using WindowsInput.Native;

namespace FuckBot.Gui.Operations.Outlast
{
    class WiggleOperation : IBotOperation
    {
        private readonly InputSimulator _inputSimulator;

        public IEnumerable<string> Keywords { get; }
        public string Name => "WigglePlayer";
        public string Description => "Wiggles the character for 5 sec!";


        public WiggleOperation(IEnumerable<string> keyword)
        {
            Keywords = keyword;
            _inputSimulator = new InputSimulator();
        }

        public void Run()
        {
            Console.WriteLine("Wiggle the player!");

            for (int i = 0; i < 100; i++)
            {
                _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.VK_Q);
                Thread.Sleep(50);
                _inputSimulator.Keyboard.KeyUp(VirtualKeyCode.VK_Q);
                _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.VK_R); 
                Thread.Sleep(50);
                _inputSimulator.Keyboard.KeyUp(VirtualKeyCode.VK_Q);
                Thread.Sleep(50);
            }
        }
    }
}
