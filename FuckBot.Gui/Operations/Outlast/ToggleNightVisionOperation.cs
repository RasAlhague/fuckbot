﻿using System;
using System.Collections.Generic;
using System.Threading;
using WindowsInput;
using WindowsInput.Native;

namespace FuckBot.Gui.Operations.Outlast
{
    class ToggleNightVisionOperation : IBotOperation
    {
        private readonly InputSimulator _inputSimulator;

        public IEnumerable<string> Keywords { get; }
        public string Name => "Toggle knight vision";
        public string Description => "Toggles the nightvision";


        public ToggleNightVisionOperation(IEnumerable<string> keyword)
        {
            Keywords = keyword;
            _inputSimulator = new InputSimulator();
        }

        public void Run()
        {
            Console.WriteLine("Toggle nightvision!");

            _inputSimulator.Keyboard.KeyPress(VirtualKeyCode.VK_F);
        }
    }
}
