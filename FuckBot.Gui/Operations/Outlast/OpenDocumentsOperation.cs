﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsInput;
using WindowsInput.Native;

namespace FuckBot.Gui.Operations.Outlast
{
    class OpenDocumentsOperation : IBotOperation
    {
        private readonly InputSimulator _inputSimulator;

        public IEnumerable<string> Keywords { get; }
        public string Name => "Open Documents";
        public string Description => "Opens the journal!";


        public OpenDocumentsOperation(IEnumerable<string> keyword)
        {
            Keywords = keyword;
            _inputSimulator = new InputSimulator();
        }

        public void Run()
        {
            Console.WriteLine("Opens the journal!");

            _inputSimulator.Keyboard.KeyPress(VirtualKeyCode.VK_J);
        }
    }
}
