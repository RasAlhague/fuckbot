﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsInput;
using WindowsInput.Native;

namespace FuckBot.Gui.Operations.Outlast
{
    class ZoomInOperation : IBotOperation
    {
        private readonly InputSimulator _inputSimulator;

        public IEnumerable<string> Keywords { get; }
        public string Name => "ZoomCameraIn";
        public string Description => "Zooms in the camera!";


        public ZoomInOperation(IEnumerable<string> keyword)
        {
            Keywords = keyword;
            _inputSimulator = new InputSimulator();
        }

        public void Run()
        {
            Console.WriteLine("Zoom in camera!");

            _inputSimulator.Mouse.VerticalScroll(10);
        }
    }
}
