﻿namespace FuckBot.Gui
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);

            _fuckBot.Dispose();
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnLoadBundle = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lblKeyword = new System.Windows.Forms.Label();
            this.lblCountFucks = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.pStatus = new System.Windows.Forms.Panel();
            this.lblBundleName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.SystemColors.GrayText;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnStart.Location = new System.Drawing.Point(12, 195);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(160, 50);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.SystemColors.GrayText;
            this.btnStop.Enabled = false;
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnStop.Location = new System.Drawing.Point(178, 195);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(160, 50);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.BtnStop_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 26);
            this.label1.TabIndex = 2;
            this.label1.Text = "Curse counter:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label2.Location = new System.Drawing.Point(7, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(235, 26);
            this.label2.TabIndex = 3;
            this.label2.Text = "Last detected keyword:";
            // 
            // btnLoadBundle
            // 
            this.btnLoadBundle.BackColor = System.Drawing.SystemColors.GrayText;
            this.btnLoadBundle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadBundle.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnLoadBundle.Location = new System.Drawing.Point(460, 195);
            this.btnLoadBundle.Name = "btnLoadBundle";
            this.btnLoadBundle.Size = new System.Drawing.Size(160, 50);
            this.btnLoadBundle.TabIndex = 4;
            this.btnLoadBundle.Text = "Load Bundle";
            this.btnLoadBundle.UseVisualStyleBackColor = false;
            this.btnLoadBundle.Click += new System.EventHandler(this.BtnLoadBundle_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label3.Location = new System.Drawing.Point(7, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 26);
            this.label3.TabIndex = 5;
            this.label3.Text = "Description:";
            // 
            // lblKeyword
            // 
            this.lblKeyword.AutoSize = true;
            this.lblKeyword.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.lblKeyword.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblKeyword.Location = new System.Drawing.Point(248, 64);
            this.lblKeyword.Name = "lblKeyword";
            this.lblKeyword.Size = new System.Drawing.Size(64, 26);
            this.lblKeyword.TabIndex = 7;
            this.lblKeyword.Text = "None";
            // 
            // lblCountFucks
            // 
            this.lblCountFucks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCountFucks.AutoSize = true;
            this.lblCountFucks.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.lblCountFucks.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblCountFucks.Location = new System.Drawing.Point(248, 9);
            this.lblCountFucks.Name = "lblCountFucks";
            this.lblCountFucks.Size = new System.Drawing.Size(24, 26);
            this.lblCountFucks.TabIndex = 6;
            this.lblCountFucks.Text = "0";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.lblDescription.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblDescription.Location = new System.Drawing.Point(248, 119);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(64, 26);
            this.lblDescription.TabIndex = 8;
            this.lblDescription.Text = "None";
            // 
            // pStatus
            // 
            this.pStatus.BackColor = System.Drawing.Color.DarkRed;
            this.pStatus.Location = new System.Drawing.Point(580, -8);
            this.pStatus.Name = "pStatus";
            this.pStatus.Size = new System.Drawing.Size(60, 60);
            this.pStatus.TabIndex = 9;
            // 
            // lblBundleName
            // 
            this.lblBundleName.AutoSize = true;
            this.lblBundleName.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblBundleName.Location = new System.Drawing.Point(457, 179);
            this.lblBundleName.Name = "lblBundleName";
            this.lblBundleName.Size = new System.Drawing.Size(0, 13);
            this.lblBundleName.TabIndex = 10;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(632, 256);
            this.Controls.Add(this.lblBundleName);
            this.Controls.Add(this.pStatus);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblKeyword);
            this.Controls.Add(this.lblCountFucks);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnLoadBundle);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnLoadBundle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblKeyword;
        private System.Windows.Forms.Label lblCountFucks;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Panel pStatus;
        private System.Windows.Forms.Label lblBundleName;
    }
}