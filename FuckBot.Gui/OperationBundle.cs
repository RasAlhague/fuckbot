﻿using FuckBot.Gui.Operations;
using FuckBot.Gui.Operations.Outlast;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Speech.Recognition;

namespace FuckBot
{
    public class OperationBundle
    {
        public class BundleInfo
        {
            public string BundleName { get; set; }
            public List<OperationInfo> Operations { get; set; }
        }

        public class OperationInfo
        {
            public string Name { get; set; }
            public List<string> Keywords { get; set; }
        }

        private readonly List<IBotOperation> _botOperations;

        public string BundleName { get; }

        public OperationBundle(string name)
        {
            _botOperations = new List<IBotOperation>();
            BundleName = name;
        }

        public IEnumerable<IBotOperation> BotOperations => _botOperations;

        public void AddOperation(IBotOperation botOperation)
        {
            if (!_botOperations.Any(x => x.GetType() == botOperation.GetType()))
                _botOperations.Add(botOperation);
        }

        public void RemoveOperation(Type type)
        {
            _botOperations.RemoveAll(x => x.GetType() == type);
        }

        public Grammar BuildGrammar()
        {
            List<string> choiceStrings = new List<string>();

            foreach (var operation in _botOperations)
            {
                foreach (var keyword in operation.Keywords)
                {
                    choiceStrings.Add(keyword);
                }
            }

            Choices choices = new Choices(choiceStrings.ToArray());

            return new Grammar(choices.ToGrammarBuilder());
        }

        /// <summary>
        /// Loads a an configuration for an Operation bundle from a file.
        /// </summary>
        /// <param name="filePath">Path of the file which contains the Operation bundle config.</param>
        /// <exception cref="FileNotFoundException">Throws a file not found exception if file path is wrong.</exception>
        /// <returns></returns>
        public static OperationBundle LoadFromFile(string filePath)
        {
            // let it throw exception and catch it underneath
            string fileContents = File.ReadAllText(filePath);

            BundleInfo bundleInfo = JsonConvert.DeserializeObject<BundleInfo>(fileContents);

            OperationBundle bundle = new OperationBundle(bundleInfo.BundleName);

            foreach (var operationInfo in bundleInfo.Operations)
            {
                bundle.AddOperation(GetOperationFromName(operationInfo.Name, operationInfo.Keywords));
            }

            return bundle;
        }

        private static IBotOperation GetOperationFromName(string name, IEnumerable<string> keywords)
        {
            switch (name)
            {
                case "DropItemsOperation":
                    return new DropItemsOperation(keywords);
                case "TerrariaTestOperation":
                    return new TerrariaTestOperation(keywords);
                case "PlayScarySoundOperation":
                    return new PlayScarySoundOperation(keywords);
                case "MoveRandomOperation":
                    return new MoveRandomOperation(keywords);
                case "ChangeWeaponOperation":
                    return new ChangeWeaponOperation(keywords);
                case "MemeMeOperation":
                    return new MemeMeOperation(keywords);
                case "ZoomInOperation":
                    return new ZoomInOperation(keywords);
                case "WiggleOperation":
                    return new WiggleOperation(keywords);
                case "ToggleNightVisionOperation":
                    return new ToggleNightVisionOperation(keywords);
                case "RotatePlayerOperation":
                    return new RotatePlayerOperation(keywords);
                case "ReloadOperation":
                    return new ReloadOperation(keywords);
                case "OpenDocumentsOperation":
                    return new OpenDocumentsOperation(keywords);
                default:
                    return null;
            }
        }
    }
}
