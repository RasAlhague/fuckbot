﻿using System.Collections.Generic;

namespace FuckBot
{
    public interface IBotOperation
    {
        string Name { get; }
        IEnumerable<string> Keywords { get; }
        string Description { get; }
        void Run();
    }
}
