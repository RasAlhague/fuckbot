﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace FuckBot.Gui
{
    public partial class MainForm : Form
    {
        private readonly FuckBot _fuckBot;
        private int _fuckCounter;

        public MainForm()
        {
            InitializeComponent();

            _fuckBot = new FuckBot(new CultureInfo("de-DE"), OperationBundle.LoadFromFile(Config.GetConfig(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "fuckbot_config.json")).DefaultBundleFolder));
            _fuckBot.FuckDetected += OnFuckDetected;
            _fuckCounter = 0;
            lblBundleName.Text = _fuckBot.BundleName;
        }

        private void OnFuckDetected(string name, string keyword, string description)
        {
            _fuckCounter++;

            lblCountFucks.Text = _fuckCounter.ToString();
            lblKeyword.Text = keyword;
            lblDescription.Text = description;

            Debug.WriteLine($"Name: {name}, keyword: {keyword}, description: {description}");
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _fuckBot.Stop();
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            _fuckBot.Run();
            pStatus.BackColor = Color.DarkGreen;

            btnStart.Enabled = false;
            btnStop.Enabled = true;
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            _fuckBot.Stop();
            pStatus.BackColor = Color.DarkRed;

            btnStart.Enabled = true;
            btnStop.Enabled = false;
        }

        private void BtnLoadBundle_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dia = new OpenFileDialog())
            {
                dia.InitialDirectory = Environment.CurrentDirectory;
                dia.Filter = "json files (*.json)|*.json";
                dia.RestoreDirectory = true;

                if (dia.ShowDialog() == DialogResult.OK)
                {

                    var bundle = OperationBundle.LoadFromFile(dia.FileName);

                    _fuckBot.ChangeBundle(bundle);
                }

                lblBundleName.Text = _fuckBot.BundleName;
            }
        }
    }
}
